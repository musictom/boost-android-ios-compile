#!/bin/bash
TARGET=android-24
ARCH=arch-arm64
#export NDK_ROOT=/Users/musictom/Library/Android/sdk/ndk-bundle
export NDK_ROOT=/home/musictom/Android/Sdk/ndk-bundle

SYSRT=$NDK_ROOT/platforms/$TARGET/$ARCH
TOOL=aarch64-linux-android
MYHOME=/Users/musictom/libs/android/$ARCH/usr
INCLUDEPTH=$MYHOME/include
LIBPTH=$MYHOME/lib
#PLATFORMS=(arm64-v8a x86_64 mips64 armeabi armeabi-v7a x86 mips)
export PLATFORMS=(arm64-v8a)
#export CC=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-gcc
#export LD=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-ld
#export CPP=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-cpp
#export CXX=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-g++
#export AS=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-as
#export AR=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-ar
#export RANLIB=$NDK_ROOT/toolchains/$TOOL-4.9/prebuilt/darwin-x86_64/bin/$TOOL-ranlib
export SYSROOT="$NDK_ROOT/platforms/$TARGET/$ARCH"
echo 'SYSROOT:'$SYSROOT
export CC=$(./ndk-which gcc $PLATFORMS) 
echo 'CC:' $CC
export LD=$(./ndk-which ld $PLATFORMS)
echo 'LD:' $LD
export CPP=$(./ndk-which cpp $PLATFORMS)
echo 'CPP:' $CPP
export CXX=$(./ndk-which g++ $PLATFORMS)
echo 'CXX:' $CXX
export AS=$(./ndk-which as $PLATFORMS)
echo 'AS:' $AS
export AR=$(./ndk-which ar $PLATFORMS)
echo 'AR:' $AR
export RANLIB=$(./ndk-which ranlib $PLATFORMS)
echo 'RANLIB:' $RANLIB

real_path() {
  [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

#Change this env variable to the number of processors you have
if [ -f /proc/cpuinfo ]; then
  JOBS=$(grep flags /proc/cpuinfo |wc -l)
elif [ ! -z $(which sysctl) ]; then
  JOBS=$(sysctl -n hw.ncpu)
else
  JOBS=2
fi

REL_SCRIPT_PATH="$(dirname $0)"
SCRIPTPATH=$(real_path $REL_SCRIPT_PATH)
ATOMICLPATH="$SCRIPTPATH/../boost"

if [ -z "$NDK_ROOT" ]; then
  echo "Please set your NDK_ROOT environment variable first"
  exit 1
fi

if [[ "$NDK_ROOT" == .* ]]; then
  echo "Please set your NDK_ROOT to an absolute path"
  exit 1
fi

export SYSROOT="$NDK_ROOT/platforms/$TARGET/$ARCH"
export CPPFLAGS="-I$NDK_ROOT/platforms/$TARGET/$ARCH/usr/include -I$INCLUDEPTH --sysroot=$SYSROOT -I$SYSRT"

export LIBS="-lssl -lcrypto"
export LDFLAGS="-L$SCRIPTPATH/obj/local/armeabi -L$LIBPTH -L$SYSRT"

EXITCODE=$?
if [ $EXITCODE -ne 0 ]; then
  echo "Error running the configure program"
  cd $PWD
  exit $EXITCODE
fi

#Build cURL
#$NDK_ROOT/ndk-build -j$JOBS -C $SCRIPTPATH boost_atomic boost_filesystem boost_chrono boost_datetime
$NDK_ROOT/ndk-build -j$JOBS -C $SCRIPTPATH
EXITCODE=$?
if [ $EXITCODE -ne 0 ]; then
	echo "Error running the ndk-build program"
	exit $EXITCODE
fi

#Strip debug symbols and copy to the prebuilt folder

DESTDIR=$SCRIPTPATH/../prebuilt/android

for p in ${PLATFORMS[*]}; do
  mkdir -p $DESTDIR/$p
  STRIP=$($SCRIPTPATH/ndk-which strip $p)
  echo '$STRIP:'
  echo $STRIP

  SRC=$SCRIPTPATH/obj/local/$p/libboost_atomic.so
  DEST=$DESTDIR/lib/$p/libboost_atomic.so
  SRC_FILESYSTEM=$SCRIPTPATH/obj/local/$p/libboost_filesystem.so
  DEST_FILESYSTEM=$DESTDIR/lib/$p/libboost_filesystem.so
  SRC_CHRONO=$SCRIPTPATH/obj/local/$p/libboost_chrono.so
  DEST_CHRONO=$DESTDIR/lib/$p/libboost_chrono.so
  SRC_DATETIME=$SCRIPTPATH/obj/local/$p/libboost_datetime.so
  DEST_DATETIME=$DESTDIR/lib/$p/libboost_datetime.so

  if [ -z "$STRIP" ]; then
    echo "WARNING: Could not find 'strip' for $p"
    cp $SRC $DEST
    cp $SRC_FILESYSTEM $DEST_FILESYSTEM
    cp $SRC_CHRONO $DEST_CHRONO
    cp $SRC_DATETIME $DEST_DATETIME
  else
    $STRIP $SRC --strip-debug -o $DEST
    $STRIP $SRC_FILESYSTEM --strip-debug -o $DEST_FILESYSTEM
    $STRIP $SRC_CHRONO --strip-debug -o $DEST_CHRONO
    $STRIP $SRC_DATETIME --strip-debug -o $DEST_DATETIME
  fi
done

#Copying cURL headers
cp -R $ATOMICLPATH/ $DESTDIR/include/
#rm $DESTDIR/include/curl/.gitignore

cd $PWD
exit 0
