BOOST_CHRONO_COMMON_CFLAGS := \
  -DANDROID -D_REENTRANT \
  -Wno-sign-compare -Wno-incompatible-pointer-types-discards-qualifiers
BOOST_CHRONO_COMMON_CFLAGS += \
  -DHAVE_CONFIG_H \
  -Wpointer-arith -Wwrite-strings -Wunused -Winline \
  -Wnested-externs -Wmissing-declarations -Wmissing-prototypes -Wno-long-long \
  -Wfloat-equal -Wno-multichar -Wno-format-nonliteral \
  -Wendif-labels -Wstrict-prototypes -Wdeclaration-after-statement \
  -Wno-system-headers -Wno-typedef-redefinition -Wno-unused-variable \
  -Wno-unused-function -I/Users/musictom/libs/android/$(ARCH)/usr/include
BOOST_CHRONO_CSOURCES := \
  chrono.cpp process_cpu_clocks.cpp thread_clock.cpp
BOOST_CHRONO_LOCAL_SRC_FILES := $(addprefix ../../curl/lib/,$(CHRONO_CSOURCES))
BOOST_CHRONO_LOCAL_C_INCLUDES += \
  $(NDK_PATH)/platforms/$(TARGET_PLATFORM)/$(ARCH)/usr/include \
  /Users/musictom/libs/android/$(ARCH)/usr/include
