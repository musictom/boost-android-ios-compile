BOOST_FILESYSTEM_COMMON_CFLAGS := \
  -DANDROID -D_REENTRANT \
  -Wno-sign-compare -Wno-incompatible-pointer-types-discards-qualifiers
BOOST_FILESYSTEM_COMMON_CFLAGS += \
  -DHAVE_CONFIG_H \
  -Wpointer-arith -Wwrite-strings -Wunused -Winline \
  -Wnested-externs -Wmissing-declarations -Wmissing-prototypes -Wno-long-long \
  -Wfloat-equal -Wno-multichar -Wno-format-nonliteral \
  -Wendif-labels -Wstrict-prototypes -Wdeclaration-after-statement \
  -Wno-system-headers -Wno-typedef-redefinition -Wno-unused-variable \
  -Wno-unused-function -I/Users/musictom/libs/android/$(ARCH)/usr/include
BOOST_FILESYSTEM_CSOURCES := \
  codecvt_error_category.cpp operations.cpp path_traits.cpp \
  path.cpp portability.cpp unique_path.cpp utf8_codecvt_facet.cpp
BOOST_FILESYSTEM_LOCAL_SRC_FILES := $(addprefix ../../curl/lib/,$(FILESYSTEM_CSOURCES))
BOOST_FILESYSTEM_LOCAL_C_INCLUDES += \
  $(NDK_PATH)/platforms/$(TARGET_PLATFORM)/$(ARCH)/usr/include \
  $(LOCAL_PATH)/../../openssl/include /Users/musictom/libs/android/$(ARCH)/usr/include
