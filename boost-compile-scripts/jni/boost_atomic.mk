BOOST_ATOMIC_COMMON_CFLAGS := \
  -DANDROID -D_REENTRANT \
  -Wno-sign-compare -Wno-incompatible-pointer-types-discards-qualifiers
BOOST_ATOMIC_COMMON_CFLAGS += \
  -DHAVE_CONFIG_H \
  -Wpointer-arith -Wwrite-strings -Wunused -Winline \
  -Wnested-externs -Wmissing-declarations -Wmissing-prototypes -Wno-long-long \
  -Wfloat-equal -Wno-multichar -Wno-format-nonliteral \
  -Wendif-labels -Wstrict-prototypes -Wdeclaration-after-statement \
  -Wno-system-headers -Wno-typedef-redefinition -Wno-unused-variable \
  -Wno-unused-function -I/Users/musictom/libs/android/$(ARCH)/usr/include
BOOST_ATOMIC_CSOURCES := \
  lockpool.cpp
BOOST_ATOMIC_LOCAL_SRC_FILES := $(addprefix ../../curl/lib/,$(CURL_CSOURCES))
BOOST_ATOMIC_LOCAL_C_INCLUDES += \
  $(LOCAL_PATH)/../../curl/include \
  $(LOCAL_PATH)/../../curl/lib \
  $(NDK_PATH)/platforms/$(TARGET_PLATFORM)/$(ARCH)/usr/include \
  $(LOCAL_PATH)/../../openssl/include /Users/musictom/libs/android/$(ARCH)/usr/include
