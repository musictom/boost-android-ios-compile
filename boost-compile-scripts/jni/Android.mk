LOCAL_PATH := $(call my-dir)
ARCH := arch-arm64
#SSL
#include $(LOCAL_PATH)/ssl.mk

#Crypto
#include $(LOCAL_PATH)/crypto.mk

#cURL
include $(LOCAL_PATH)/boost_atomic.mk
include $(LOCAL_PATH)/boost_filesystem.mk
include $(LOCAL_PATH)/boost_chrono.mk
include $(LOCAL_PATH)/boost_datetime.mk

#Static libssl
#include $(CLEAR_VARS)
#LOCAL_MODULE := ssl
#LOCAL_SRC_FILES := $(SSL_LOCAL_SRC_FILES)
#LOCAL_C_INCLUDES := $(SSL_LOCAL_C_INCLUDES)
#LOCAL_CPPFLAGS += -std=c++11
#include $(LOCAL_PATH)/optimizations.mk
#LOCAL_CFLAGS += $(SSL_COMMON_CFLAGS)
#include $(BUILD_STATIC_LIBRARY)

#Static libcrypto
#include $(CLEAR_VARS)
#LOCAL_MODULE := crypto
#LOCAL_SRC_FILES := $(CRYPTO_LOCAL_SRC_FILES)
#LOCAL_C_INCLUDES := $(CRYPTO_LOCAL_C_INCLUDES)
#LOCAL_CPPFLAGS += -std=c++11
#include $(LOCAL_PATH)/optimizations.mk
#LOCAL_CFLAGS += $(CRYPTO_COMMON_CFLAGS)
#include $(BUILD_STATIC_LIBRARY)

#libboost_atomic.so
include $(CLEAR_VARS)
LOCAL_PATH := $(ROOT_PATH)/../
LOCAL_MODULE := boost_atomic
LOCAL_SRC_FILES := $(BOOST_ATOMIC_LOCAL_SRC_FILES)
LOCAL_C_INCLUDES := $(BOOST_ATOMIC_LOCAL_C_INCLUDES)
LOCAL_CPPFLAGS += -std=c++11 -I/home/musictom/libs/android/$(ARCH)/usr/include
LOCAL_LDFLAGS += -L/home/musictom/libs/android/$(ARCH)/usr/lib
include jni/optimizations.mk
LOCAL_CFLAGS += $(BOOST_ATOMIC_COMMON_CFLAGS)
include $(BUILD_SHARED_LIBRARY)

#libboost_filesystem.so
include $(CLEAR_VARS)
LOCAL_PATH := $(ROOT_PATH)/../
LOCAL_MODULE := boost_filesystem
LOCAL_SRC_FILES := $(BOOST_FILESYSTEM_LOCAL_SRC_FILES)
LOCAL_C_INCLUDES := $(BOOST_FILESYSTEM_LOCAL_C_INCLUDES)
LOCAL_CPPFLAGS += -std=c++11 -I/home/musictom/libs/android/$(ARCH)/usr/include
LOCAL_LDFLAGS += -L/home/musictom/libs/android/$(ARCH)/usr/lib
include jni/optimizations.mk
LOCAL_CFLAGS += $(BOOST_FILESYSTEM_COMMON_CFLAGS)
include $(BUILD_SHARED_LIBRARY)

#libboost_chrono.so
include $(CLEAR_VARS)
LOCAL_PATH := $(ROOT_PATH)/../
LOCAL_MODULE := boost_chrono
LOCAL_SRC_FILES := $(BOOST_CHRONO_LOCAL_SRC_FILES)
LOCAL_C_INCLUDES := $(BOOST_CHRONO_LOCAL_C_INCLUDES)
LOCAL_CPPFLAGS += -std=c++11 -I/home/musictom/libs/android/$(ARCH)/usr/include
LOCAL_LDFLAGS += -L/home/musictom/libs/android/$(ARCH)/usr/lib
include jni/optimizations.mk
LOCAL_CFLAGS += $(BOOST_CHRONO_COMMON_CFLAGS)
include $(BUILD_SHARED_LIBRARY)

#libboost_datetime.so
include $(CLEAR_VARS)
LOCAL_PATH := $(ROOT_PATH)/../
LOCAL_MODULE := boost_datetime
LOCAL_SRC_FILES := $(BOOST_DATETIME_LOCAL_SRC_FILES)
LOCAL_C_INCLUDES := $(BOOST_DATETIME_LOCAL_C_INCLUDES)
LOCAL_CPPFLAGS += -std=c++11 -I/home/musictom/libs/android/$(ARCH)/usr/include
LOCAL_LDFLAGS += -L/home/musictom/libs/android/$(ARCH)/usr/lib
include jni/optimizations.mk
LOCAL_CFLAGS += $(BOOST_DATETIME_COMMON_CFLAGS)
include $(BUILD_SHARED_LIBRARY)