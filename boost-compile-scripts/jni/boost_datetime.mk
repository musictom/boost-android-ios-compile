BOOST_DATETIME_COMMON_CFLAGS := \
  -DANDROID -D_REENTRANT \
  -Wno-sign-compare -Wno-incompatible-pointer-types-discards-qualifiers
BOOST_DATETIME_COMMON_CFLAGS += \
  -DHAVE_CONFIG_H \
  -Wpointer-arith -Wwrite-strings -Wunused -Winline \
  -Wnested-externs -Wmissing-declarations -Wmissing-prototypes -Wno-long-long \
  -Wfloat-equal -Wno-multichar -Wno-format-nonliteral \
  -Wendif-labels -Wstrict-prototypes -Wdeclaration-after-statement \
  -Wno-system-headers -Wno-typedef-redefinition -Wno-unused-variable \
  -Wno-unused-function -I/Users/musictom/libs/android/$(ARCH)/usr/include
BOOST_DATETIME_CSOURCES := \
  gregorian/date_generators.cpp gregorian/greg_month.cpp \
  gregorian/greg_names.hpp gregorian/greg_weekday.cpp gregorian/gregorian_types.cpp \
  posix_time/posix_time_types.cpp
BOOST_DATETIME_LOCAL_SRC_FILES := $(addprefix ../../curl/lib/,$(DATETIME_CSOURCES))
BOOST_DATETIME_LOCAL_C_INCLUDES += \
  $(NDK_PATH)/platforms/$(TARGET_PLATFORM)/$(ARCH)/usr/include \
  /Users/musictom/libs/android/$(ARCH)/usr/include
